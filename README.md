# community-decides.
a simple reddit bot that deletes submissions if they reach a certain score.


# this is an outdated version
if you wish to use the bot, please use the github version. [https://github.com/tempsafeguard/community-decides]



# disclaimer
i'm still a beginner with python. this code is messy and may not function 100% correctly. please start an issue or a pull request if you wish to help.

# !!WARNING!!
Please be careful if you use this on a live subreddit. The bot is experimental and may not work fully. Try on a test subreddit first.


# what is it?
it's a simple reddit bot. it comments on every new submission. if the comments gets to a certain score or below, it'll automatically be removed. the bot constantly checks the new queue and the list of comments. all comments are saved to a local db.


# to-do list
* save post ids [✅] - **completed**. the bot grabs the comment ids from a local database and adds them to a local variable. it then reads from these local variables. when a thread is deleted, the comment id is removed from the database and local variable.

* automatically pin comment [✅] - **completed**. the bot now pins the comment and distinguishes itself.

* add comments [✅] - **completed**. code easier to understand.


# how do i use it?
(1) download this repo
(2) install the requirements
(3) run (it will automatically create a database)
